const {watch, series} = require("gulp");
const browserSync = require('browser-sync').create();

const styles = require("./styles");
const pages = require("./pages");
const images = require("./images");
const scripts = require("./scripts");

const {html, scss, img, js} = require("./paths");

const watchFiles = ()=> {
    watch(html.src, pages);
    watch(scss.src, styles);
    watch(img.src, images);
    watch(js.src, scripts);
}

module.exports = watchFiles;