const browserSync = require('browser-sync').create();

const server = (done) => {
    browserSync.init({
        server: {
            baseDir: './dist/pages/'
        },
        port: 3000
    });
    done();
}

module.exports = server;